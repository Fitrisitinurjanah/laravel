<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class authController extends Controller
{
    public function pendataan()
    {
        return view('halaman.register');
    }
    public function wellcome(Request $request)
    {
        // dd($request->all());
        $first = $request['first'];
        $last = $request['last'];

        return view('halaman.wellcome', compact('first', 'last'));
    }
}
