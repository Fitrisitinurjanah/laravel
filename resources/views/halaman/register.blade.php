@extends('layout.master')
@section('judul')
    <h2>Buat Account Baru</h2>
@endsection
@section('content')
    <h4>Sign Up Form</h4>
    <form action="/send" method="post">
    @csrf
    <label> First Name </label><br>
    <input type="text" name="first"><br>
    <label> Last Name </label><br>
    <input type="text" name="last"><br>
    <label> Gender</label><br>
    <input type="radio" name="Gender" value="1">Male<br>
    <input type="radio" name="Gender" value="2">Famale<br><br>
    <label>Nationaly</label><br>
    <select name="nasional" id="nasional"><br>
        <option value="Indonesia">Indonesia</option>
        <option value="Singapura">Singapura</option>
        <option value="Malaysia">Malaysia</option>
    </select><br><br>
    <label> Language Spoken</label><br>
    <input type="checkbox" name="bahasa"> Indonesia<br>
    <input type="checkbox" name="bahasa"> Inggris<br>
    <input type="checkbox" name="bahasa"> Amerika<br>
    <label> Bio</label><br>
    <textarea name="pesan" id="pesan" cols="30" rows="10"></textarea><br>
    <input type="Submit" value="Sign Up">
@endsection